﻿using System;
namespace currencyconverter.AuthorizationModule
{
    public class PasswordValidator: IValidator
    {
        public bool Validate(string str)
        {
            return !string.IsNullOrEmpty(str);
        }
    }
}
