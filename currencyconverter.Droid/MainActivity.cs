﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using currencyconverter.AuthorizationModule;
using System.Threading.Tasks;
using Android.Content;
using Android.Views.InputMethods;
using Android.Views;

namespace currencyconverter.Droid
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        Button LoginButton;
        EditText LoginField;
        EditText PasswordField;
        string Login;
        string Password;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);
            LoginButton = FindViewById<Button>(Resource.Id.loginButton);
            LoginField = FindViewById<EditText>(Resource.Id.loginField);
            PasswordField = FindViewById<EditText>(Resource.Id.passwordField);

            // try to make my softkeyboard hiding
            var imm = GetSystemService(Context.InputMethodService) as InputMethodManager;
            imm.ShowSoftInput(LoginButton, InputMethodManager.ShowImplicit);
            FindViewById<LinearLayout>(Resource.Id.mainLayout).Click += (sender, e) =>
            {
                imm.HideSoftInputFromWindow((sender as View).WindowToken, 0);
            };

            LoginButton.Click += (sender, e) => {
                Login = LoginField.Text;
                Password = PasswordField.Text;
                var LoginValidator = new LoginValidator();
                var PasswordValidator = new PasswordValidator();
                var AuthSender = new AuthSender();
                var Interactor = new AuthorizationInteractor(LoginValidator, PasswordValidator, AuthSender);
                var result = Interactor.Login(Login, Password).Result;

                var message = GetMessage(result);
                ShowAlert(message);
            };
        }

        private void ShowAlert(string message)
        {
            Android.App.AlertDialog.Builder alert = new Android.App.AlertDialog.Builder(this);
            alert.SetTitle("");
            alert.SetMessage(message);
            alert.SetPositiveButton("OK", (senderAlert, args) => { });
            Dialog dialog = alert.Create();
            dialog.Show();
        }

        private string GetMessage(EAuthResult result)
        {
            string message = "";
            switch (result)
            {
                case EAuthResult.InvalidData:
                    {
                        message = "Login or password coldn't be blank!";
                        break;
                    }
                case EAuthResult.Success:
                    {
                        message = "Authenticated successfully!";
                        break;
                    }
                case EAuthResult.Unauthorized:
                    {
                        message = "Your login or password is incorrect! Try again!";
                        break;
                    }
                default: break;
            }
            return message;
        }
    }
    class AuthSender : IAuthSender
    {
        public async Task<bool> SendAuthRequest(string login, string pass)
        {
            if (login == "admin" && pass == "admin")
            {
                return true;
            }
            return false;
        }
    }
}